const menuMock = {
  menu: [
    {
      name: "Usuarios",
      icon: "fas fa-users",
      badge: undefined,
      menu: [
        {
          name: "Crear usuarios",
          icon: "fas fa-user-plus",
          badge: 0,
          menu: undefined,
        },
        {
          name: "Modificar usuarios",
          icon: "fas fa-user-edit",
          badge: 0,
          menu: undefined,
        },
        {
          name: "Usuarios borrados",
          icon: "fas fa-user-minus",
          badge: 0,
          menu: undefined,
        },
      ],
    },
    {
      name: "Chat",
      icon: "fas fa-comment-dots",
      badge: 3,
      menu: [
        {
          name: "Mensajes",
          icon: "fas fa-comments",
          badge: 3,
          menu: undefined,
        },
        {
          name: "Amigos en línea",
          icon: "fas fa-user-friends",
          badge: 9,
          menu: undefined,
        },
        {
          name: "Comunicados",
          icon: "fas fa-envelope",
          badge: 0,
          menu: undefined,
        },
      ],
    },
    {
      name: "Mi cuenta",
      icon: "fas fa-user",
      badge: 3,
      menu: [
        {
          name: "Configuración",
          icon: "fas fa-cog",
          badge: 0,
          menu: [
            {
              name: "Contraseña",
              icon: "",
              badge: undefined,
              menu: undefined,
            },
            {
              name: "Correo",
              icon: "",
              badge: undefined,
              menu: undefined,
            },
            {
              name: "Eliminar cuenta",
              icon: "",
              badge: undefined,
              menu: undefined,
            },
          ],
        },
        {
          name: "Mis datos personales",
          icon: "fas fa-user-shield",
          badge: 0,
          menu: undefined,
        },
        {
          name: "Cerrar sesión",
          icon: "fas fa-sign-out-alt",
          badge: 0,
          menu: undefined,
        },
      ],
    },
  ],
};

module.exports = menuMock;