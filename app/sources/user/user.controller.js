const User = require('./user.model');
const bcrypt = require('bcrypt');
const JWT = require('../../jwt');
const UserSession = require('./../session/user_session.controller');

exports.getList = function(params, response){
    User.findAll()
        .then(data => response.status(200).send(data))
        .catch(error => response.status(500).send(error))
}

exports.login = function({query:{ username,password }}, response){
    bcrypt.hash("1234", 0,(error, value) => {})
    User.findOne({
        where: { username }
    })
        .then(data => {
            bcrypt.compare(password, data.password, (error, check) => {
                if(check){
                    var token = JWT.createToken(data.id);
                    console.log(token)
                    response.status(200).send( {token} );

                }
                else{
                    response.status(200).send( false );
                }
            })
        })
        .catch(error => response.status(400).send(false))
    
}

exports.register = function({query: {name, username, password}}, response){
    bcrypt.hash(password,0, (error, value)=>{
        User.create({
            name: name,
            username: username,
            password: value
        }).then(data => {
            response.status(200).send(true);
        }).catch(error => {response.status(500).send(error)});
    })
}

exports.logout = function({query: {token}}, response ){
    console.log('***************'+token)
    var {id} = JWT.decodeToken(token);
    console.log('***************'+id)

    UserSession.delete(id).then().catch(error => console.log(error));
}