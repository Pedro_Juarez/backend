const app = require('express')();
const server = require('http').createServer(app);
const cors = require('cors')();
const routes = require('./app/routes');
const socketIO = require('./app/socket-io');

const port = process.env.PORT || 7431;

/*  UPS  */
app.use(cors);
routes.set(app);

socketIO.init(server);
server.listen(port, () => {
    console.log('Express ready: http://127.0.0.1:'+port);
})